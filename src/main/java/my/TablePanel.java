package my;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TablePanel extends JPanel
{
    private MyTableModel tm = null;
    private JTable tbl = null;
    //private Functions_H2 func = null;
    private PersonDAO_Hibernate func = null;
    JFrame frame;

    public TablePanel(JFrame frame)
    {
        setLayout(null);
        setBackground(Color.GRAY.darker());
        this.frame = frame;
        tm = new MyTableModel();
        tbl = new JTable(tm);
        //func = new Functions_H2();
        func = new PersonDAO_Hibernate();
        tbl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(tbl);
        scroll.setBounds(10, 10, 350, 350);
        add(scroll);

        JButton btnLoad = new JButton("Load");
        btnLoad.setBounds(380, 30, 90, 20);
        btnLoad.addActionListener(new buttonActionLoad());
        add(btnLoad);

        JButton btnNew = new JButton("New");
        btnNew.setBounds(380, 70, 90, 20);
        btnNew.addActionListener(new buttonActionNew());
        add(btnNew);

        JButton btnEdit = new JButton("Edit");
        btnEdit.setBounds(380, 110, 90, 20);
        btnEdit.addActionListener(new buttonActionEdit());
        add(btnEdit);

        JButton btnDelete = new JButton("Delete");
        btnDelete.setBounds(380, 150, 90, 20);
        btnDelete.addActionListener(new buttonActionDelete());
        add(btnDelete);
    }

    //----------------------------------------------------------

    //----------------------------------------------------------
    class buttonActionLoad implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae)
        {
            DialogPerson pd = new DialogPerson();
            tm.pp = func.read(pd.ppd.getPerson());
            tbl.revalidate();

        }
    }

    //----------------------------------------------------------
    class buttonActionNew implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae)
        {
            DialogPerson pd = new DialogPerson();
            //pd.setLabelText("Inter Functions_H2 and update:");
            pd.setModal(true);
            pd.setVisible(true);
            func.add(pd.ppd.getPerson());
        }
    }

    //----------------------------------------------------------
    class buttonActionEdit implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae)
        {
            DialogPerson pd = new DialogPerson();
            //pd.setLabelText("Inter Functions_H2 and update:");
            pd.setModal(true);
            pd.setVisible(true);
            func.update(pd.ppd.getPerson());
        }
    }

    //----------------------------------------------------------
    class buttonActionDelete implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae)
        {
            Person p = tm.pp.get(tbl.getSelectedRow());
            func.delete(p);
        }
    }
}	
