package my;


import java.sql.SQLException;
import java.util.ArrayList;

public interface PersonDAO
{

    void add(Person person) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;

    ArrayList<Person> read(Person person) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;

    void update(Person person) throws ClassNotFoundException, SQLException;

    void delete(Person person) throws IllegalAccessException, ClassNotFoundException, SQLException;

    String dbLocation = "jdbc:h2:file:db/mydb";
    String dbUserName = "sa";
    String dbPass = "";
}
