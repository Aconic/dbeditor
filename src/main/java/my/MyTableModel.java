package my;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

class MyTableModel extends AbstractTableModel
{
    ArrayList<Person> pp = new ArrayList<Person>();

    @Override
    public int getColumnCount()
    {
        return 4;
    }

    @Override
    public String getColumnName(int column)
    {
        String ret = "";
        switch (column)
        {
            case 0:
                ret = "Id";
                break;
            case 1:
                ret = "First Name";
                break;
            case 2:
                ret = "Last Name";
                break;
            case 3:
                ret = "Age";
                break;
        }
        return ret;
    }

    @Override
    public int getRowCount()
    {
        return pp.size();
    }

    @Override
    public Object getValueAt(int ri, int ci)
    {
        Object ret = null;
        Person p = pp.get(ri);

        switch (ci)
        {
            case 0:
                ret = p.getId();
                break;
            case 1:
                ret = p.getFName();
                break;
            case 2:
                ret = p.getLName();
                break;
            case 3:
                ret = p.getAge();
                break;
        }
        return ret;
    }

}
