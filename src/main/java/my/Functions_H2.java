package my;


import java.sql.*;
import java.util.ArrayList;

public class Functions_H2 implements PersonDAO
{
	public void add(Person p) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
		Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection(dbLocation,dbUserName, dbPass);
		Statement st = conn.createStatement();
		st.executeUpdate("insert into person values("+p.getId()+", '"+ p.getFName()+"', '"+ p.getLName()+"', "+p.getAge()+")");
		st.close();
		conn.close();
	}

	@Override
	public ArrayList<Person> read (Person person) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{	
		ArrayList<Person> pp = new ArrayList<Person>();
		Class.forName("org.h2.Driver");
		Connection conn = DriverManager.getConnection(dbLocation, dbUserName, dbPass);
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM Person");

		while( rs.next() )
		{
            Person pers = new Person(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			pp.add(pers);
		    System.out.println( pp );
		}
		st.close();
		conn.close();
		rs.close();

		return pp;
	}



	public ArrayList<Person> readAll () throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
		ArrayList<Person> pp = new ArrayList<Person>();
		Class.forName("org.h2.Driver");
		Connection conn = DriverManager.getConnection(dbLocation, dbUserName, dbPass);
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM Person");

		while( rs.next() )
		{
			Person pers = new Person(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			pp.add(pers);
			System.out.println( pp );
		}
		st.close();
		conn.close();
		rs.close();

		return pp;
	}




	@Override
	public void update(Person np) throws ClassNotFoundException, SQLException
	{
		Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection(dbLocation, dbUserName, dbPass);
		PreparedStatement st = conn.prepareStatement("update person as p set p.fname= ?, lname= ?, age=?	"
				+"where id=?");
        String query = "Update PERSON set Fname = '" + np.getFName()
                + "',Lname = '" + np.getLName() + "',Age = " + np.getAge()
                + " where id=" + np.getId();
        st.executeUpdate(query);;
		st.executeUpdate();
		st.close();
		conn.close();
	}

	@Override
	public void delete(Person np) throws  IllegalAccessException, ClassNotFoundException, SQLException
	{
		Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection(dbLocation, dbUserName, dbPass);
        Statement st = conn.createStatement();
        String query = "Delete From PERSON where id=" + np.getId();
        st.executeUpdate(query);
        st.close();
        conn.close();
	}
}