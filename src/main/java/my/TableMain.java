package my;

import javax.swing.*;

public class TableMain
{
	public static void main(String[] args) 
	{
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					// 0 => "javax.swing.plaf.metal.MetalLookAndFeel"
					// 3 => the Windows Look and Feel
					String name = UIManager.getInstalledLookAndFeels()[3].getClassName();
					UIManager.setLookAndFeel(name);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				new TableFrame();
			}
		});
	}

}