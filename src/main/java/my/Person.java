package my;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Person")
public class Person implements Serializable
{
    int id;
    String firstName;
    String lastName;
    int age;

    public Person(int anInt, String string, String rsString, int rsInt)
    {
        id = anInt;
        firstName = string;
        lastName = rsString;
        age = rsInt;
    }
    public Person()
    {
        id = 0;
        firstName = "";
        lastName = "";
        age = 0;
    }
    //@Access(AccessType.PROPERTY)
    //@ElementCollection(targetClass=Phone.class)
    //@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="Functions_H2")
    //@OneToMany(mappedBy="Functions_H2")
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="Functions_H2")
//    @JoinTable(
//            name="Functions_H2",
//            joinColumns = @JoinColumn( name="id"),
//            inverseJoinColumns = @JoinColumn( name="person_id")
//    )

    //@OneToMany(mappedBy="Functions_H2")
    //@OneToMany
    //@JoinColumn(name="person_id")


    @Id
    @Column(name = "Id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Column(name = "FName")
    public String getFName()
    {
        return firstName;
    }

    public void setFName(String firstName)
    {
        this.firstName = firstName;
    }

    @Column(name = "LName")
    public String getLName()
    {
        return lastName;
    }

    public void setLName(String lastName)
    {
        this.lastName = lastName;
    }

    @Column(name = "Age")
    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    @Override
    public String toString()
    {
        String str = id + ", " + firstName + ", " + lastName + ", " + age;
        return str;
    }
}
