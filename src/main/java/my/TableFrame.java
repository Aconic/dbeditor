package my;

import javax.swing.JFrame;

public class TableFrame extends JFrame 
{
	public TableFrame()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100,100, 500, 400);
		add(new TablePanel(this));
		setVisible(true);
	}
}
