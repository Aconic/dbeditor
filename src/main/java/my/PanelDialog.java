package my;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class PanelDialog extends JPanel 
{

	private JTextField fiD = null;
	private JTextField fFn = null;
	private JTextField fLn = null;
	private JTextField fAge= null;

	private Person p = null;
	private String labelText ="";

	//-------------------------------------------------------
	public void setLabelText(String text)
	{
		labelText = text;
	}
	//-------------------------------------------------------

	public Person getPerson ()
	{
		return p;
	}
	//-------------------------------------------------------

	public PanelDialog() 
	{
		setLayout(null);

		JLabel liD = new JLabel("ID:");
		liD.setBounds(20, 20, 100, 20);
		add (liD);

		JLabel lFl = new JLabel("First Name:");
		lFl.setBounds(20, 60, 100, 20);
		add (lFl);

		JLabel lLl = new JLabel("Last Name:");
		lLl.setBounds(20, 100, 100, 20);
		add (lLl);

		JLabel lAge = new JLabel("Age:");
		lAge.setBounds(20, 140, 100, 20);
		add (lAge);

		JLabel lButtonText = new JLabel(labelText, JLabel.CENTER);
		lButtonText.setBounds(100, 230, 200, 20);
		add (lButtonText);

		fiD = new JTextField ();
		fiD.setBounds(120, 20, 60, 20);
		add (fiD);

		fFn = new JTextField ();
		fFn.setBounds(120, 60, 120, 20);
		add (fFn);

		fLn = new JTextField ();
		fLn.setBounds(120, 100, 120, 20);
		add (fLn);

		fAge = new JTextField ();
		fAge.setBounds(120, 140, 40, 20);
		add (fAge);

		JButton btnOk = new JButton("OK");
		btnOk.setBounds(0, 200, 350, 20);
		btnOk.addActionListener(new buttonAction());
		add(btnOk);
	}	

	//-------------------------------------------------	

	class buttonAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			p = new Person(Integer.parseInt(fiD.getText()),
					fFn.getText(),
					fLn.getText(),
					Integer.parseInt(fAge.getText()));
		}
	}
}









