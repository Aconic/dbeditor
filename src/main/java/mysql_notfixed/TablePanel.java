package mysql_notfixed;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class TablePanel extends JPanel
{
	private MyTableModel tm = null;
	private JTable tbl = null;


	public TablePanel() 
	{
		setLayout(null);
		tm = new MyTableModel();
		tbl = new JTable(tm);
		tbl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scroll = new JScrollPane (tbl);
		scroll.setBounds(10, 10, 350, 350);
		add (scroll);

		JButton btnLoad = new JButton("Load");
		btnLoad.setBounds(380, 30, 90, 20);
		btnLoad.addActionListener( new buttonActionLoad());
		add(btnLoad);	

		JButton btnNew = new JButton("New");
		btnNew.setBounds(380, 70, 90, 20);
		btnNew.addActionListener( new buttonActionNew());
		add(btnNew);	

		JButton btnEdit = new JButton("Edit");
		btnEdit.setBounds(380, 110, 90, 20);
		btnEdit.addActionListener( new buttonActionEdit());
		add(btnEdit);

		JButton btnDelete = new JButton("delete");
		btnDelete.setBounds(380, 150, 90, 20);
		btnDelete.addActionListener( new buttonActionDelete());
		add(btnDelete);
	}

	//----------------------------------------------------------	

	//----------------------------------------------------------	
	class buttonActionLoad implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			try
			{
				tm.pp = Functions_H2.Read();
				tbl.revalidate();
			} 
			catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			
		}
	}


	//----------------------------------------------------------
	class buttonActionNew implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			try 
			{
				DialogPerson pd = new DialogPerson();
				//pd.setLableText("Inter Functions_H2 and update:");
				pd.setModal(true);
				pd.setVisible(true);
				Functions_H2.create(pd.ppd.getPerson());
			} 
			catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) 
			{
				e.printStackTrace();
			}
		}
	}


	//----------------------------------------------------------
	class buttonActionEdit implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{	
			try 
			{
				DialogPerson pd = new DialogPerson();
				//pd.setLableText("Inter Functions_H2 and update:");
				pd.setModal(true);
				pd.setVisible(true);
				Functions_H2.Update(pd.ppd.getPerson());
			} 
			catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) 
			{
				e.printStackTrace();
			}
		}
	}

	//----------------------------------------------------------
	class buttonActionDelete implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			try 
			{
				Functions_H2 p = tm.pp.get(tbl.getSelectedRow());
				Functions_H2.delete(p);
			}
			catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) 
			{
				e.printStackTrace();
			}
		}
	}
}	
