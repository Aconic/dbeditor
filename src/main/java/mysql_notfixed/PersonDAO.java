package mysql_notfixed;



import java.util.ArrayList;

public interface PersonDAO
{
	
	public void add(Person person);
	public ArrayList<Person> read(Person person);
	public void update(Person person);
	public void delete(int id);
}
