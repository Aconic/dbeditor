package mysql_notfixed;


import java.sql.*;
import java.util.ArrayList;

public class Functions_H2 implements PersonDAO
{
	 final static String dbFolder = "";

	public static void create(Person p) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/mysql","root", "");
		Statement st = conn.createStatement();
		st.executeUpdate("insert into person values("+p.getId()+", '"+ p.getFName()+"', '"+ p.getLName()+"', "+p.getAge()+")");
		st.close();
		conn.close();
	}


	public static ArrayList<Person> Read () throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
		ArrayList<Person> pp = new ArrayList<Person>();
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/mysql","root", "");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM Person");

		while( rs.next() )
		{
            Person p = new Person( rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			pp.add(p);
		    System.out.println( p );
		}
		st.close();
		conn.close();
		rs.close();

		return pp;
	}

	public static void Update(Person np) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/mysql","root", "");
		PreparedStatement st = conn.prepareStatement("update person as p set p.fname= ?, lname= ?, age=?	"
				+"where id=?");
        String query = "Update PERSON set Fname = '" + np.getFName()
                + "',Lname = '" + np.getLName() + "',Age = " + np.getAge()
                + " where id=" + np.getId();
        st.executeUpdate(query);;
		st.executeUpdate();
		st.close();
		conn.close();
	}


	public static void delete(Person np) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/mysql","root", "");
        Statement st = conn.createStatement();
        String query = "Delete From PERSON where id=" + np.getId();
        st.executeUpdate(query);
        st.close();
        conn.close();
	}

	@Override
	public void add(Person person)
	{

	}

	@Override
	public ArrayList<Person> read(Person person)
	{
		return null;
	}

	@Override
	public void update(Person person)
	{

	}

	@Override
	public void delete(int id)
	{

	}
}