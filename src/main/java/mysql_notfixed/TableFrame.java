package mysql_notfixed;

import javax.swing.*;

public class TableFrame extends JFrame 
{
	public TableFrame()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100,100, 500, 400);
		add(new TablePanel());
		setVisible(true);
	}
}
