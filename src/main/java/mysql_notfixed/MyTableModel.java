package mysql_notfixed;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

class MyTableModel extends AbstractTableModel
{
	ArrayList<Functions_H2> pp = new ArrayList<Functions_H2>();

	@Override
	public int getColumnCount() 
	{
		return 4;
	}

	@Override
	public String getColumnName(int column) 
	{
		String ret = "";
		switch (column) 
		{
		case 0:
			ret = "Id";
		case 1:
			ret =  "FirstName";
		case 2:
			ret =  "LasstName";
		case 3:
			ret =  "Age";
		}
		return ret;
	}

	@Override
	public int getRowCount() 
	{
		return pp.size();	
	}

	@Override
	public Object getValueAt(int ri, int ci) 
	{
		Object ret = null;
		Functions_H2 p=pp.get(ri);

		switch (ci) 
		{
		case 0:    ret = p.getId(); 	break;
		case 1:    ret = p.getFName(); 	break;
		case 2:    ret = p.getLName(); 	break;
		case 3:    ret = p.getAge(); 	break;
		}
		return ret;
	}

}
