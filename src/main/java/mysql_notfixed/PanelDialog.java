package mysql_notfixed;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class PanelDialog extends JPanel 
{

	private JTextField fiD = null;
	private JTextField fFn = null;
	private JTextField fLn = null;
	private JTextField fAge= null;

	private JButton btnAdd= null;
	//private JButton btnEdit= null;
	//private JButton btnDelete= null;

	private Functions_H2 p = null;
	private String lableText="Button";  

	//-------------------------------------------------------
	public void setLableText (String text) 
	{
		lableText = text;
	}
	//-------------------------------------------------------

	public Functions_H2 getPerson ()
	{
		return p;
	}
	//-------------------------------------------------------

	public PanelDialog() 
	{
		setLayout(null);

		JLabel liD = new JLabel("ID:");
		liD.setBounds(20, 20, 100, 20);
		add (liD);

		JLabel lFl = new JLabel("First Name:");
		lFl.setBounds(20, 60, 100, 20);
		add (lFl);

		JLabel lLl = new JLabel("Last Name:");
		lLl.setBounds(20, 100, 100, 20);
		add (lLl);

		JLabel lAge = new JLabel("Age:");
		lAge.setBounds(20, 140, 100, 20);
		add (lAge);



		JLabel lButtonText = new JLabel(lableText, JLabel.CENTER);
		lButtonText.setBounds(100, 230, 200, 20);
		add (lButtonText);



		fiD = new JTextField ();
		fiD.setBounds(120, 20, 220, 20);
		add (fiD);

		fFn = new JTextField ();
		fFn.setBounds(120, 60, 220, 20);
		add (fFn);

		fLn = new JTextField ();
		fLn.setBounds(120, 100, 220, 20);
		add (fLn);

		fAge = new JTextField ();
		fAge.setBounds(120, 140, 220, 20);
		add (fAge);

		btnAdd = new JButton("Press");
		btnAdd.setBounds(150, 250, 100, 20);
		btnAdd.addActionListener( new buttonAction());
		add(btnAdd);	
	}	

	//-------------------------------------------------	

	class buttonAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			p = new Functions_H2(Integer.parseInt(fiD.getText()),
							fFn.getText(),  
							fLn.getText(), 
							Integer.parseInt(fAge.getText()));
		}			
	}
}









