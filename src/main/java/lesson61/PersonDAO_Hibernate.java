package lesson61;

import org.hibernate.Session;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class PersonDAO_Hibernate implements PersonDAO
{

	@Override
	public  void  add(Person person) {

		Session session = null;
		try {

			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(person);
			session.getTransaction().commit();


		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
			//           session.getTransaction().rollback();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public ArrayList<Person> read(Person person) 
	{	
		List<Person> persons = new ArrayList<Person>();

		Session session = null;
		try {

			session = HibernateUtil.getSessionFactory().openSession();

			if (person == null) 
			{
				persons = session.createCriteria(Person.class).list();
			}

			else  persons.add((Person) session.load(Person.class, person.getId()));

		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O",JOptionPane.OK_OPTION);
		} 
		finally 
		{
			if (session != null && session.isOpen()) 
			{
				session.close();
			}
		}
		return (ArrayList<Person>) persons;
	}



	@Override
	public void update(Person person) {

		Session session = null;
		try {

			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(person);
			session.getTransaction().commit();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(int id) {

		Session session = null;
		Person p = new Person();
		p.setId(id);

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(p);
			session.getTransaction().commit();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}  
}