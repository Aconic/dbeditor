package lesson61;

import java.sql.*;
import java.util.ArrayList;


public class PersonDAO_H2 implements PersonDAO
{
	@Override
	public void add(Person person) 
	{
		System.out.println("add");	
	}

	public ArrayList<Person> read(Person person) 
	{
		 ArrayList<Person> personList =  new  ArrayList<>();
		
		if(person == null){
			try {
				Class.forName("org.h2.Driver");
				//Connection conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "sa", "");
				Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
		        
		        Statement st = conn.createStatement();
		        ResultSet result;
		        result = st.executeQuery("select * from person");
		        
		        while (result.next())
		        {
		        	Person personNew = new Person(); 
		        	
		        	personNew.setId(result.getInt("ID"));
		        	personNew.setFirstName(result.getString("FNAME"));
		        	personNew.setLastName(result.getString("LNAME"));
		        	personNew.setAge(result.getInt("AGE"));
		        	personList.add(personNew);
		        }
		        st.close();
		        conn.close();
			} catch (ClassNotFoundException | SQLException e) 
			{
				e.printStackTrace();
			}
			
		}else{
			System.out.println("read");
		}
		
		
		return personList;
	}

	public void update(Person person) 
	{
		System.out.println("update");
		
	}

	public void delete(int id) {
		System.out.println("delete");
		
	}

}
