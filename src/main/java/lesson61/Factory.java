package lesson61;


public class Factory {

	private static PersonDAO dao = null;

	public static PersonDAO getI(){

		if (dao == null)
		{
			dao = new PersonDAO_Hibernate();
			//dao = new Functions_H2();
		}
		return dao;
	}  
}