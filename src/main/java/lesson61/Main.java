package lesson61;



import java.sql.SQLException;
import java.util.List;


public class Main {

	public static void main(String[] args) throws SQLException 
	{
		List<Person> lst = Factory.getI().read(null);
		System.out.println("=========================================");
		for (Person person : lst) 
		{
			System.out.println(person);
		}
	}    
}