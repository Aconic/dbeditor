package lesson61;

//import java.util.List;
//import java.util.Set;

import javax.persistence.*;
import java.io.Serializable;

//import org.hibernate.mapping.List;

//import org.hibernate.annotations.CascadeType;
//import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.annotations.IndexColumn;


@Entity
@Table(name="Person")
public class Person implements Serializable
{
	
	int id;
	String firstName;
	String lastName;
	int age;
	
	public Person() {
		id = 0;
		firstName = "";
		lastName = "";
		age = 0;
	}
	//@Access(AccessType.PROPERTY)
	//@ElementCollection(targetClass=Phone.class)
	//@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="Functions_H2")
	//@OneToMany(mappedBy="Functions_H2")
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="Functions_H2")
//    @JoinTable(
//            name="Functions_H2",
//            joinColumns = @JoinColumn( name="id"),
//            inverseJoinColumns = @JoinColumn( name="person_id")
//    )
	
	//@OneToMany(mappedBy="Functions_H2")
	//@OneToMany
	//@JoinColumn(name="person_id")

	
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    @Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="FName")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="LName")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="age")
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() 
	{
		String str = id + ", " + firstName + ", " + lastName + ", " + age;
		return str;
	}
}
